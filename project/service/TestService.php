<?php
/**
 * Created by PhpStorm.
 * User: kaervanon
 * Date: 16.05.18
 * Time: 9:23
 */

namespace project\service;


use project\entity\Author;
use project\entity\Language;
use project\entity\Post;
use project\helper\Util;

class TestService
{
    private $headers = [
        1 => ["жесть", "удивительно", "снова", "совсем", "шок", "случай", "сразу", "событие", "начало", "вирус"],
        2 => ["currency", "amazing", "again", "absolutely", "shocking", "case", "immediately", "event", "beginning", "virus"]
    ];

    private $texts = [
        1 => ["один", "еще", "бы", "такой", "только", "себя", "свое", "какой", "когда", "уже", "для", "вот", "кто", "да", "говорить", "год", "знать", "мой", "до", "или", "если", "время", "рука", "нет", "самый", "ни", "стать", "большой", "даже", "другой", "наш", "свой", "ну", "под", "где", "дело", "есть", "сам", "раз", "чтобы", "два", "там", "чем", "глаз", "жизнь", "первый", "день", "тута", "во", "ничто", "потом", "очень", "со", "хотеть", "ли", "при", "голова", "надо", "без", "видеть", "идти", "теперь", "тоже", "стоять", "друг", "дом", "сейчас", "можно", "после", "слово", "здесь", "думать", "место", "спросить", "через", "лицо", "что", "тогда", "ведь", "хороший", "каждый", "новый", "жить", "должный", "смотреть", "почему", "потому", "сторона", "просто", "нога", "сидеть", "понять", "иметь", "конечный", "делать", "вдруг", "над", "взять", "никто", "сделать"],
        2 => ["one", "yet", "would", "such", "only", "yourself", "his", "what", "when", "already", "for", "behold", "Who", "yes", "speak", "year", "know", "my", "before", "or", "if", "time", "arm", "no", "most", "nor", "become", "big", "even", "other", "our", "his", "well", "under", "where", "a business", "there is", "himself", "time", "that", "two", "there", "than", "eye", "a life", "first", "day", "mulberry", "in", "nothing", "later", "highly", "with", "to want", "whether", "at", "head", "need", "without", "see", "go", "now", "also", "stand", "friend", "house", "now", "can", "after", "word", "here", "think", "a place", "ask", "across", "face", "what", "then", "after all", "good", "each", "new", "live", "due", "look", "why", "because", "side", "just", "leg", "sit", "understand", "have", "finite", "do", "all of a sudden", "above", "to take", "no one", "make"]
    ];


    private $countHeaders;
    private $countWords;
    private $date_start = 1483228800;
    private $date_end = 1504915200;
    private $authors;
    private $languages;

    private $countAuthors;
    private $countLangs;

    private $sort_array;



    public function __construct()
    {

        $this->authors = Author::find()->all();
        $this->languages = Language::find()->all();
        $this->countAuthors = count($this->authors);
        $this->countLangs = count($this->languages);

    }

    public function fillPostsTable()
    {
        $remaining = $this->getRemaining();
        $this->initSortArray();
        $posts = [];
        for($i = 1; $i <= $remaining; $i++){
            $author_key = mt_rand(0, $this->countAuthors - 1);
            $lang_key = mt_rand(0, $this->countLangs - 1);
            $lang = $this->languages[$lang_key];
            $author = $this->authors[$author_key];
            $this->countHeaders = count($this->headers[$lang->getPrimaryKey()]) - 1;
            $this->countWords = count($this->texts[$lang->getPrimaryKey()]) - 1;

            $date = mt_rand($this->date_start, $this->date_end);
            $header = $this->createSentence(4, 6, $this->headers[$lang->getPrimaryKey()], $this->countHeaders);
            $header = Util::mb_ucfirst($header);
            $text = $this->createText(3, 4, $this->texts[$lang->getPrimaryKey()], $this->countWords);
            $text = Util::mb_ucfirst($text);
            $likes = ceil(100/sqrt($this->sort_array[$i]));

            $post = Post::create($header, $text, $date, $likes);
            $post->setAuthor($author);
            $post->setLanguage($lang);
            $posts[] = $post->attributes;
        }

        $postModel = new Post;
        \Yii::$app->db->createCommand()->batchInsert(Post::tableName(), $postModel->attributes(), $posts)->execute();
    }

    private function initSortArray()
    {
        $range = range(1, 10000);
        shuffle($range);
        $range2 = range(1, 10000);
        $this->sort_array = array_combine($range2, $range);
    }


    private function getRemaining()
    {
        $postCount = Post::find()->count();
        return 10000 - $postCount;
    }



    private function createSentence($from, $to, array $src, $wordsInSrc)
    {
        $words = [];
        $countWords = mt_rand($from, $to);

        for($hw = 1; $hw <= $countWords; $hw++){
            $word_key = mt_rand(0, $wordsInSrc);
            $words[] = $src[$word_key];
        }
        $words = implode(' ', $words);

        return $words;
    }

    private function createText($from, $to, array $src, $wordsInSrc){
        $text = [];
        $sentences = mt_rand($from, $to);
        for($s = 1; $s <= $sentences; $s++) {
            $sentence = $this->createSentence(5, 8, $src, $wordsInSrc);
            $sentence = Util::mb_ucfirst($sentence).'.';
            $text[] = $sentence;
        }
        $text = implode(' ', $text);
        return $text;
    }



}