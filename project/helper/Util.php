<?php
/**
 * Created by PhpStorm.
 * User: kaervanon
 * Date: 16.05.18
 * Time: 10:17
 */

namespace project\helper;


class Util
{
    public static function mb_ucfirst($string, $enc = 'UTF-8')
    {
        return mb_strtoupper(mb_substr($string, 0, 1, $enc), $enc) .
            mb_substr($string, 1, mb_strlen($string, $enc), $enc);
    }
}