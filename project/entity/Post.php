<?php
namespace project\entity;

use yii\db\ActiveRecord;

/**
 * Class Post
 * @package project\entity
 * @property integer $id
 * @property integer $created_at
 * @property string $header
 * @property string $text
 * @property string $likes_counter
 */
class Post extends ActiveRecord
{
    public static function create($header, $text, $date, $likes): self
    {
        $post = new self();
        $post->header = $header;
        $post->text = $text;
        $post->created_at = $date;
        $post->likes_counter = $likes;
        return $post;
    }

    public function setAuthor(Author $author)
    {
        $this->author_id = $author->getPrimaryKey();
    }

    public function setLanguage(Language $language)
    {
        $this->lang_id = $language->getPrimaryKey();
    }


    public static function tableName()
    {
        return '{{%Posts}}';
    }

}