<?php
namespace project\entity;

use yii\db\ActiveRecord;

/**
 * Class Post
 * @package project\entity
 * @property integer $id
 * @property integer $created_at
 * @property string $header
 * @property string $text
 * @property string $likes_counter
 */
class Author extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%Authors}}';
    }

}