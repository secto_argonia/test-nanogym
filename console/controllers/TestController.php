<?php
namespace console\controllers;

use project\service\TestService;
use yii\base\Module;
use yii\console\Controller;
use yii\console\ExitCode;

class TestController extends Controller
{
    private $service;

    public function __construct($id, Module $module, TestService $service, array $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }


    public function actionIndex()
    {
        $this->service->fillPostsTable();
        $this->stdout("done".PHP_EOL);
        return ExitCode::OK;
    }



}