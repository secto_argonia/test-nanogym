<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Authors}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ], $tableOptions);

        $this->createTable('{{%Languages}}', [
                'id' => $this->primaryKey(),
                'name' => $this->string()
            ], $tableOptions);

        $this->createTable('{{%Posts}}',[
            'id' => $this->primaryKey(),
            'lang_id' => $this->integer(),
            'author_id' => $this->integer(),
            'created_at' => $this->integer(),
            'header' => $this->string(),
            'text' => $this->text(),
            'likes_counter' => $this->integer()
        ]);

        $this->createIndex('{{%idx-posts-lang_id}}', '{{%Posts}}', 'lang_id');
        $this->createIndex('{{%idx-posts-author_id}}', '{{%Posts}}', 'author_id');

        $this->addForeignKey('{{%fk-posts-lang_id}}', '{{%Posts}}', 'lang_id', '{{%Languages}}', 'id', 'CASCADE');
        $this->addForeignKey('{{%fk-posts-author_id}}', '{{%Posts}}', 'author_id', '{{%Authors}}', 'id', 'CASCADE');

        $names_array = ["CrazyNews", "Чук и Гек", "CatFuns", "CarDriver", "BestPics", "ЗОЖ", "Вася Пупкин", "Готовим со вкусом", "Шахтёрская Правда", "FunScience"];
        foreach($names_array as $name) {
            $this->insert('{{%Authors}}', [
                'id' => null,
                'name' => $name,
            ]);
        }

        $langs_array = ['Русский', 'English'];
        foreach ($langs_array as $lang) {
            $this->insert('{{%Languages}}', [
                'id' => null,
                'name' => $lang,
            ]);
        }

    }

    public function down()
    {
        $this->dropForeignKey('{{%fk-posts-lang_id}}', '{{%Posts}}');
        $this->dropForeignKey('{{%fk-posts-author_id}}', '{{%Posts}}');

        $this->dropTable('{{%Authors}}');
        $this->dropTable('{{%Languages}}');
        $this->dropTable('{{%Posts}}');
    }
}
